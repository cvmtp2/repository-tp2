
/*-------------DROP---------------*/
DROP INDEX idx;
DROP TABLE vocabulaire;
DROP TABLE matrice;





/*--------------------------------*/



/*Mots sans doublons*/
CREATE TABLE vocabulaire(
	id NUMBER(10),
  motv VARCHAR(50),
	
	CONSTRAINT pk_vocabulaire PRIMARY KEY (id)
);



/*Matrice*/
CREATE TABLE matrice(

  id_gauche NUMBER(10),
	id_droite NUMBER(10),
	resultat NUMBER(20),
    TF Number(20)
	
);
CREATE INDEX idx ON MATRICE (id_gauche, id_droite, resultat);


