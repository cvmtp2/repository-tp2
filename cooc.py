#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import re
import numpy as np
import cx_Oracle
from collections import OrderedDict

#fonction qui lit un fichier texte		
def lireTexte(path, enc):
	f = open(path, 'r', encoding=enc)
	corpus = f.read()	
	if corpus[0] == '\ufeff':
		corpus=corpus[1:]
	f.close()	
	return corpus

#fonciton pour envoyer touts les mots des textes dans une liste
def parseMot(corpus):	
	corpus = corpus.lower()
	p = re.compile('[ ,\n\'’«».:;\?!""#%$*/\-@|<>+_`{}&¨=°()^\[\]]+')
	liste1 = p.split(corpus)
	del liste1[len(liste1) - 1]	
	return liste1

#fonction qui donne pour chaque mot de la liste une valeur unique
def accesDictionnaireBD(connexion):
	dictionnaireBD = {}
	cur = connexion.cursor()	
	selectIndexDico = "Select id, motv from vocabulaire"
	cur.execute(selectIndexDico)
	for rangee in cur.fetchall():
		mot = rangee[1]
		id = rangee[0]
		dictionnaireBD[mot]=id	
	return dictionnaireBD
		
def accesMatriceCooc(connexion, tailleDico, tailleFenetre ):
	matrice = np.zeros((tailleDico, tailleDico))
	cur = connexion.cursor()
	tf = int(tailleFenetre)	
	selectIndexListe = "Select id_gauche, id_droite, resultat from matrice where TF = :1 "
	cur.execute(selectIndexListe, {'1': tf})	
	for rangee in cur.fetchall():
		id_gauche = rangee[0]
		id_droite = rangee[1]
		resultat = rangee[2]
		matrice[id_gauche][id_droite] = resultat	
	return matrice

def accesDictionnaireAncien(connexion):
	dictionnaire = {}
	cur = connexion.cursor()	
	select = "Select id_gauche, id_droite, tf from matrice"
	cur.execute(select)	
	for rangee in cur.fetchall():
		dictionnaire[(rangee[0], rangee[1], rangee[2])] = 0	
	return dictionnaire
	
	
def indexMotDictionnaire1(liste, dicitonnaireBD):	
	motarajouterDico = []
	for i in liste:
		if i not in motarajouterDico and i not in dicitonnaireBD:
			motarajouterDico.append(i)	
	return motarajouterDico	
	
#fonction qui insert les mots sans doublon dans la base des donnnees
def insertionDictionnaire(connexion,motarajouterDico):
	cur = connexion.cursor()
	liste = []	
	selectIndexDico = "Select count(*) from vocabulaire"
	cur.execute(selectIndexDico)
	for rangee in cur.fetchall():
		index = rangee[0]
		index = int(index)
		break
	start = index
	for i in range(0, len(motarajouterDico)):
		tuple = (index, motarajouterDico[i])
		liste.append(tuple)
		index += 1
	insertBD = "INSERT INTO vocabulaire VALUES(:1,:2)"
	cur.executemany(insertBD,liste )
	connexion.commit()
	return start
#fonction qui calcule la matrice de coocurence

def connexionOracle():
	PATH_ORACLE = 'C:\Oracle\client\product\12.2.0\client_1\bin'
	sys.path.append(PATH_ORACLE)
	mdp = 'lee123'
	dns_tns = cx_Oracle.makedsn('delta', 1521, 'decinfo')
	chaineConnexion = 'e1529743' + '/' + mdp + '@' + dns_tns
	connexion = cx_Oracle.connect(chaineConnexion)
	return connexion;	

def deconnexionOracle(connexion):
	connexion.close()
	
def aggrandirmatriceBD(connexion, taillefentre, liste):
	cur = connexion.cursor()
	listeA = []
	listeB = []
	tf = int(taillefentre)
	dicAncien = liste[0]
	dicNouveau = liste[1]
	for cle, valeur in dicNouveau.items():
		listeA.append(( cle[0], cle[1] , valeur, cle[2]))
	for cle, valeur in dicAncien.items():
		listeB.append(( valeur, cle[0], cle[1] , cle[2]))
	insertBD = "INSERT INTO matrice VALUES(:1,:2, :3, :4)"
	cur.executemany(insertBD,listeA )
	updateBD = "UPDATE matrice SET resultat = resultat + :1 where id_gauche = :2 and id_droite = :3 and TF = :4"
	cur.executemany(updateBD,listeB)
	connexion.commit()	
	
def matriceCoocurence(connexion,dictionnaireBD, dictionnaireAncien, liste, tailleFenetre):
	dicAncien = dictionnaireAncien
	dicNouveau = {}	
	#Initialisation des valeurs 
	TF =  int(tailleFenetre) #taille fenetre
	tailleDico = len(dictionnaireBD)
	tailleCorpus = len(liste)
	d = 0 
	m = TF//2
	F = TF	
	matrice = np.zeros((tailleDico,tailleDico))
	iMot = 0
	iccooc = 0
	#Calcul
	while F <= tailleCorpus:
		i = d
		motM = liste[m]
		iccooc = dictionnaireBD[motM]#id_g
		while i < F:
			if i != m:
				motI = liste[i]
				iMot = dictionnaireBD[motI]#id_g
				if((iMot, iccooc, TF) in dicAncien):
					dicAncien[(iMot, iccooc, TF)] += 1
				else:
					if((iMot, iccooc, TF) not in dicNouveau):
						dicNouveau[(iMot, iccooc, TF)] = 1
					else:
						dicNouveau[(iMot, iccooc, TF)] += 1				
				matrice[iMot][iccooc] +=  1
			i+= 1 
		d += 1
		m += 1
		F += 1
	list = []
	list.append(dicAncien)
	list.append(dicNouveau)
	return list
	
#fonction pour prédire avec la méthode du produit scalaire	
def predictionScalaire(dictionnaire, matrice, mot):		
	if(mot in dictionnaire):
		dictionnaireResultat = {}
		vecteurMot = matrice[dictionnaire[mot]]
		for cle in dictionnaire.keys():
			if(cle != mot):
				vecteurCle = matrice[dictionnaire[cle]]
				score = np.multiply(vecteurMot, vecteurCle)
				dictionnaireResultat[cle] = np.sum(score)
		dictionnaireTrié = OrderedDict(sorted(dictionnaireResultat.items(), key=lambda t: t[1], reverse = True))
		return dictionnaireTrié;
		
#fonction pour prédire avec la méthode least square	
def predictionLeastSquare(dictionnaire, matrice, mot):	
	if(mot in dictionnaire):
		dictionnaireResultat = {}	
		vecteurMot = matrice[dictionnaire[mot]]
		for cle in dictionnaire.keys():
			if(cle != mot):
				vecteurCle = matrice[dictionnaire[cle]]
				score = np.subtract(vecteurMot, vecteurCle)
				score = score * score
				dictionnaireResultat[cle] = np.sum(score)
		dictionnaireTrié= OrderedDict(sorted(dictionnaireResultat.items(), key=lambda t: t[1]))	
		return dictionnaireTrié;

#fonction pour prédire avec la méthode city block	
def predictionCityBlock(dictionnaire, matrice, mot):	
	if(mot in dictionnaire):
		dictionnaireResultat = {}
		vecteurMot = matrice[dictionnaire[mot]]
		for cle in dictionnaire.keys():
			if(cle != mot):
				vecteurCle = matrice[dictionnaire[cle]]
				score = np.subtract(vecteurMot, vecteurCle)
				score = np.absolute(score)
				dictionnaireResultat[cle] = np.sum(score)
		dictionnaireTrié = OrderedDict(sorted(dictionnaireResultat.items(), key=lambda t: t[1]))	
		return dictionnaireTrié	
	
#fonction pour afficher sur l'écran les synonmes
def affichageSynonyme(dictionnaireResultat, mot, nbSynonyme):
	if(mot in stopListe()):
		print("\nCe mot est un stopword.")
	else:
		print("\nLes synonymes du mot " + mot + " sont : \n")
		indexSynonyme = 0
		for key, value in dictionnaireResultat.items():
			if(key in  stopListe()):
				continue
			print(key + ', ' + str(value))
			if(indexSynonyme == nbSynonyme - 1):
				break
			indexSynonyme += 1

def stopListe():
    f = open('StopListe.txt','r',encoding = 'utf-8')
    stopList = f.read()
    f.close()    
    return stopList

def main():
	entrainement = False
	tailleFenetre = ""
	recherche = False
	connexion = connexionOracle()
	for i in range(1, len(sys.argv)):
		if(sys.argv[i] == "-e"):
			entrainement = True
			enc = ""
			chemin = []
			break
		elif(sys.argv[i] == "-s"):
			recherche = True
			break	
	if(entrainement == True):
		for i in range(1, len(sys.argv)):
			if(sys.argv[i] == "-e"):
				continue
			elif(sys.argv[i] == "-t"):
				tailleFenetre = sys.argv[i + 1]
			elif(sys.argv[i] == "-enc"):
				enc = sys.argv[i + 1]
			elif(sys.argv[i] == "-cc"):
				for j in range (i+1, len(sys.argv)):
					if(sys.argv[j] != "-cc" and sys.argv[j] != "-enc" and sys.argv[j] != "-t" and sys.argv[j] != "-e"):
						chemin.append(sys.argv[j])
					else:
						break						
		listeMot = []		
		#On va chercher tous les mots de tout les textes pour les mettre dans une liste
		for i in range(0, len(chemin)):
			texte = lireTexte(chemin[i], enc)
			listeTemp = parseMot(texte)
			listeMot.extend(listeTemp)		
		dictionnaireAncien = accesDictionnaireAncien(connexion)
		dictionnaireBD = accesDictionnaireBD(connexion)
		listeDictionnaireMot = indexMotDictionnaire1(listeMot, dictionnaireBD)
		start = insertionDictionnaire(connexion, listeDictionnaireMot)		
		dictionnaireBD = accesDictionnaireBD(connexion)
		liste = matriceCoocurence(connexion, dictionnaireBD, dictionnaireAncien, listeMot, tailleFenetre)
		aggrandirmatriceBD(connexion, tailleFenetre, liste)				
		print("entrainement fini")
	elif(recherche == True):
		for i in range(1,len(sys.argv)):
			if(sys.argv[i] == "-s"):
				continue
			elif(sys.argv[i] == "-t"):
				tailleFenetre = sys.argv[i + 1]		
		#Initialisation des valeurs nécéssaires
		phrase = ''
		mot=''	
		nbSynonyme = ''
		methode = ''
		Begin = True
		End = False
		bonInput = False		
		dictionnaireBD = accesDictionnaireBD(connexion)
		matrice = accesMatriceCooc(connexion, len(dictionnaireBD) , tailleFenetre)	
		while (End != True):			
			#Assignation de la méthode a utiliser
			if(Begin == False and bonInput == True):
				if(methode == 0):
					fonction = predictionScalaire
				elif(methode == 1):
					fonction = predictionLeastSquare
				elif(methode == 2):
					fonction = predictionCityBlock
				dictionnaireResultat = fonction(dictionnaireBD, matrice, mot)
				affichageSynonyme(dictionnaireResultat, mot, nbSynonyme)
			else:
				Begin = False			
			#Affichage de l'application
			phrase = input("\nEntrez un mot, le nombre de synonymes que vous voulez et la méthode de calcul,\ni.e. produit scalaire: 0, lest squares: 1, cityblock: 2\n\nTapez q pour quitter\n\n")
			if(phrase == 'q' or phrase == 'Q'):
				End = True
			else:
				phrase = phrase.split()
				if(len(phrase) == 3):#Vérifie si l'usager entre le bon nombre d'argument
					mot = phrase[0]
					nbSynonyme = int(phrase[1])
					methode = int(phrase[2])
					if(methode == 0 or methode == 1 or methode == 2 ): #Vérifie si l'usager entre la bonne méthode
						bonInput = True	
					else:
						print("\nMéthode n'existe pas !")
						bonInput = False
				else: 
					print("\nPas assez ou trop d'arguments !")
					bonInput = False
	deconnexionOracle(connexion)	
if __name__ == '__main__':
	main()